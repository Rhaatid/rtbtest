/* это можно через ProvidePlugin webpack'a подцеплять, но пока просто не стал этого делать */
import $ from 'jquery'
import 'whatwg-fetch'
import 'es6-promise-promise'

/* main constants */
const APIUrl = '/project/api/registrationHandler.php',
      $form = $('#reg-form'),
      fields = $form.find('input'),
      checkboxTerms = $('#check-terms');

const ErrorVisible = errorElement => {
    errorElement.fadeIn('slow');
    let timer = setTimeout( () => { errorElement.fadeOut() }, 3000 );
    errorElement.click( () => {
        clearTimeout(timer);
        errorElement.fadeOut();
        errorElement.siblings('input').focus();
    });
};

const Validation = field => {
    let fieldID = field.prop('id');

    let regexpTextCharacters = /^([\w .!#$%^&*'@:=+?`~{}|()])*$/,
        regexpEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/,
        errors,
        fieldErrorElement = $("label[for='"+ fieldID +"']");

    switch (fieldID) {
        case 'field-name':

            let trimmedString = field.val().replace(/^\s+|\s+$/g, ''),
                validName;

            if (trimmedString) {
                validName = regexpTextCharacters.test(trimmedString);
                if ( validName ) { fieldErrorElement.text(''); errors = false; }
                else { fieldErrorElement.text('Please enter correct login'); ErrorVisible(fieldErrorElement); errors = true; }
            }
            else { fieldErrorElement.text('Please enter correct login'); ErrorVisible(fieldErrorElement); errors = true; }

            break;
        case 'field-email':

            let validEmail = regexpEmail.test(field.val());

            if( validEmail ) { fieldErrorElement.text(''); errors = false; }
            else { fieldErrorElement.text('Please enter correct email'); ErrorVisible(fieldErrorElement); errors = true; }

            break;
        case 'field-pass':
            break;
        case 'check-terms':

            if( field.prop('checked') ) { fieldErrorElement.removeClass('error'); errors = false; }
            else { fieldErrorElement.addClass('error'); errors = true; }

            field.click(() => {
                fieldErrorElement.removeClass('error');
            });

            break;
        default:
            break;
    }

    return errors;
};

$form.submit(event => {
    event.preventDefault();
    let errorFields,
        errorCheckbox,
        formData = {};

    /*validation*/
    for ( let field in fields.serializeArray() ) {
        let $field = $(fields[field]);
        if( $field.prop( 'required' ) ) { errorFields = Validation($field); }
    }
    errorCheckbox = Validation(checkboxTerms);

    /*if valid*/
    if ( !errorFields && !errorCheckbox ) {

        /* first variation */
        for ( let field in fields.serializeArray() ) {
            let $field = $(fields[field]);

            if ($field.prop('type') === 'checkbox') { formData[$field.prop('name')] = $field.prop('checked'); }
            else { formData[$field.prop('name')] = $field.val(); }
        }

        $.ajax({
            url: APIUrl,
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: data => {
                if( data ) { console.log( data ); $form[0].reset(); }
            }
        });

        /* second variation */
        // let formData = new FormData($form[0]);
        // fetch(APIUrl, {
        //     method: 'post',
        //     body: formData
        // })
        //     .then(
        //          response => {
        //              if (response.status !== 200) {
        //                  console.log('Captain we have a problems. Status Code: ' + response.status);
        //               }
        //
        //              response.json().then(data => {
        //                  console.log(data);
        //              });
        //
        //              $form[0].reset();
        //          }
        //     )
        //     .catch(function(error) {
        //         console.log('Captain we lost: ' + error);
        //     });

    } else return false;
});