<?php
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Registration</title>
        <link rel="stylesheet" href="./build/css/test.css">
    </head>
    <body>
        <div class="main">
            <div class="top">
                <a class="top__logo" href="#"></a>
                <button>Log in</button>
            </div>
            <form id="reg-form" class="registration" method="post" action="">
                <h1>Get started for free</h1>
                <h2>No credit card required</h2>
                <div id="reg-fields" class="fields">
                    <div class="fields__element fields__element_name">
                        <input id="field-name"
                               name="login"
                               placeholder="Name"
                               required
                               maxlength="100"
                               type="text">
                        <label for="field-name"></label>
                    </div>
                    <div class="fields__element fields__element_email">
                        <input id="field-email"
                               name="email"
                               required
                               placeholder="Work email"
                               type="email">
                        <label for="field-email"></label>
                    </div>
                    <div class="fields__element fields__element_pass">
                        <input id="field-pass"
                               name="password"
                               placeholder="Password"
                               required
                               maxlength="100"
                               type="password">
                        <label for="field-pass"></label>
                    </div>
                </div>


                <div class="checker">
                    <input id="check-terms" name="terms" type="checkbox" />
                    <label for="check-terms"></label>
                    <span>I agree to RealtimeBoard <a href="#">Terms Of Service</a> and <a href="#">Privacy Policy</a> </span>
                </div>

                <div class="checker">
                    <input id="check-updates" name="updates" type="checkbox" />
                    <label for="check-updates"></label>
                    <span>I agree to receive news and product updates from RealtimeBoard</span>
                </div>

                <button type="submit">Get started now</button>

                <div class="signup">
                    <h3>or sign up with services:</h3>
                    <div>
                        <a href="" class="signup__social" id="sign-office"></a>
                        <a href="" class="signup__social" id="sign-slack"></a>
                        <a href="" class="signup__social" id="sign-google"></a>
                        <a href="" class="signup__social" id="sign-facebook"></a>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="./build/bundles/registration.js"></script>

    </body>
    </html>
<?