'use strict';

let gulp  =           require('gulp'),
    watch  =          require('gulp-watch'),
    prefixer  =       require('gulp-autoprefixer'),
    uglify  =         require('gulp-uglify'),
    sass  =           require('gulp-sass'),
    sourcemaps  =     require('gulp-sourcemaps'),
    cssmin  =         require('gulp-minify-css'),
    imagemin  =       require('gulp-imagemin'),
    pngquant  =       require('imagemin-pngquant'),
    svgo  =           require('gulp-svgo'),
    webpackStream  =  require('webpack-stream'),
    named          =  require('vinyl-named'),
    spritesmith =     require('gulp.spritesmith'),
    buffer =          require('vinyl-buffer'),
    mergeStream =     require("merge-stream");

let rootPath = './project/';

let path = {
    build: {
        js:           rootPath + 'build/bundles/',
        css:          rootPath + 'build/css/',
        images:       rootPath + 'build/images/',
        fonts:        rootPath + 'build/fonts/',
        svg:          rootPath + 'build/svg/',
        sprites:      rootPath + 'build/sprites/',
        spritesSCSS:  rootPath + 'sources/styles/sprites/',
    },
    src: {
        js:             rootPath + 'sources/js/**/*.js',
        styles:         rootPath + 'sources/styles/*.scss',
        images:         rootPath + 'sources/images/**/*.*',
        fonts:          rootPath + 'sources/fonts/**/*.*',
        svg:            rootPath + 'sources/svg/**/*.*',
        sprites:        rootPath + 'sources/sprites/**/*.png',
        spritesRetina:  rootPath + 'sources/sprites/**/*2x.png'
    },
    watch: {
        js:       rootPath + 'sources/js/**/*.js',
        styles:   rootPath + 'sources/styles/**/*.scss',
        images:   rootPath + 'sources/images/**/*.*',
        fonts:    rootPath + 'sources/fonts/**/*.*',
        svg:      rootPath + 'sources/svg/**/*.*',
        sprites:  rootPath + 'sources/sprites/**/*.*'
    },
    clean: rootPath + 'build'
};

gulp.task('bundles:build' ,() => {
   gulp.src(path.src.js)
       .pipe(named())
       .pipe(webpackStream({
           module: {
               loaders: [
                   {
                       test: /\.(js)$/,
                       loader: 'babel-loader',
                       query: {
                           presets: [
                               require.resolve('babel-preset-es2015')
                           ]
                       }
                   }
               ]
           }
       }))
       .pipe(uglify())
       .pipe(gulp.dest(path.build.js))
});

gulp.task('styles:build', function () {
    gulp.src(path.src.styles)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
});

gulp.task('images:build', function () {
    gulp.src(path.src.images)
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.images))
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('svg:build', function() {
    gulp.src(path.src.svg)
        .pipe(svgo())
        .pipe(gulp.dest(path.build.svg));
});

gulp.task('sprites:build', function() {
    let spriteData = gulp.src(path.src.sprites)
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.scss',

            imgPath: '/project/build/sprites/sprite.png',
            retinaImgPath: '/project/build/sprites/sprite@2x.png',
            retinaSrcFilter: path.src.spritesRetina,
            retinaImgName: 'sprite@2x.png',
        }));

    let imgStream = spriteData.img
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.sprites));

    let cssStream = spriteData.css
        .pipe(gulp.dest(path.build.spritesSCSS));

    return mergeStream(imgStream, cssStream);
});

gulp.task('build', [
    'bundles:build',
    'styles:build',
    'fonts:build',
    'images:build',
    'svg:build'
]);

gulp.task('watch', function(){
    watch([path.watch.styles], function(event, cb) {
        gulp.start('styles:build');
    });

    watch([path.watch.js], function(event, cb) {
        gulp.start('bundles:build');
    });

    watch([path.watch.images], function(event, cb) {
        gulp.start('images:build');
    });

    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });

    watch([path.watch.svg], function(event, cb) {
        gulp.start('fonts:svg');
    });
});